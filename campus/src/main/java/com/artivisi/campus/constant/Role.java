package com.artivisi.campus.constant;

public enum Role {
    ROLE_ADMIN, ROLE_STUDENT, ROLE_TEACHER
}
