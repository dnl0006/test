package com.artivisi.campus.config;

import com.artivisi.campus.constant.Role;
import com.artivisi.campus.dao.UserDao;
import com.artivisi.campus.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class DefaultUser {

    @Autowired private UserDao userDao;
    @Autowired private PasswordEncoder passwordEncoder;

    @PostConstruct
    public void  insertUser() {
        User defaultUser = userDao.findByUsername("admin.app@gmail.com");
        if (defaultUser == null) {
            User user = new User();
            user.setUsername("admin.app@gmail.com");
            user.setRole(Role.ROLE_ADMIN);
            user.setPassword(passwordEncoder.encode("password"));
            user.setActive(true);
            userDao.save(user);
        }
    }
}
