package com.artivisi.campus.controller;

import com.artivisi.campus.dao.SubjectDao;
import com.artivisi.campus.dao.TeacherDao;
import com.artivisi.campus.entity.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping("/teacher")
public class TeacherController {

    @Autowired private TeacherDao teacherDao;
    @Autowired private SubjectDao subjectDao;

    @GetMapping("/list")
    public String showList(@RequestParam(required = false) String nik, String name,
                           ModelMap modelMap, @PageableDefault Pageable pageable) {
        if (StringUtils.hasText(nik)) {
            modelMap.addAttribute("datas",
                    teacherDao.findByNikContainingIgnoreCase(nik, pageable));
        } if (StringUtils.hasText(name)) {
            modelMap.addAttribute("datas",
                    teacherDao.findByNameContainingIgnoreCase(name, pageable));
        } else {
            modelMap.addAttribute("datas", teacherDao.findAll(pageable));
        }
        return "teacher/list";
    }

    @GetMapping("/form")
    public String showForm(@RequestParam(required = false) String id, ModelMap modelMap) {
        if (StringUtils.hasText(id)) {
            modelMap.addAttribute("teacher", teacherDao.findById(id).get());
        } else {
            modelMap.addAttribute("teacher", new Teacher());
        }
        modelMap.addAttribute("subjects", subjectDao.findAll());
        return "teacher/form";
    }

    @PostMapping("/form")
    public String processForm(@Valid Teacher teacher, BindingResult bindingResult, ModelMap modelMap, RedirectAttributes redirectAttributes) {


        if (bindingResult.hasErrors()) {
            modelMap.addAttribute("subjects", subjectDao.findAll());
            modelMap.addAttribute("teacher", teacher);
            return "teacher/form";
        }

        //validate teacher nik
        Teacher dupeCode = teacherDao.findByNik(teacher.getNik());
        if (dupeCode != null && !dupeCode.getId().equalsIgnoreCase(teacher.getId())) {
            modelMap.addAttribute("errorMessage", "NIK duplicate form");

            modelMap.addAttribute("subjects", subjectDao.findAll());
            modelMap.addAttribute("teacher", teacher);
            return "teacher/form";
        }

        try {
            teacherDao.save(teacher);
            redirectAttributes.addFlashAttribute("successMessage", "Data saved");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "redirect:/teacher/list";
    }

    @GetMapping("/delete")
    public String deleteForm(@RequestParam String id, RedirectAttributes redirectAttributes) {
        try {
            teacherDao.deleteById(id);
            redirectAttributes.addFlashAttribute("successMessage", "Data is deleted");
        } catch (Exception e) {
            e.printStackTrace();
            redirectAttributes.addFlashAttribute("errorMessage", "Error deleteing data");
        }
        return "redirect:/teacher/list";
    }
}
