package com.artivisi.campus.controller;

import com.artivisi.campus.dao.SubjectDao;
import com.artivisi.campus.entity.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;


@Controller
@RequestMapping("/subject")
public class SubjectController {

    @Autowired private SubjectDao subjectDao;

    @GetMapping("/list")
    public String showList(@RequestParam(required = false) String code,
                           ModelMap modelMap, @PageableDefault Pageable pageable) {
        if (StringUtils.hasText(code)) {
            modelMap.addAttribute("datas",
                    subjectDao.findByCodeContainingIgnoreCase(code, pageable));
        } else {
            modelMap.addAttribute("datas", subjectDao.findAll(pageable));
        }
        return "subject/list";
    }

    @GetMapping("/form")
    public String showForm(@RequestParam(required = false) String id, ModelMap modelMap) {
        if (StringUtils.hasText(id)) {
            modelMap.addAttribute("subject", subjectDao.findById(id).get());
        } else {
            modelMap.addAttribute("subject", new Subject());
        }
        return "subject/form";
    }

    @PostMapping("/form")
    public String processForm(@Valid Subject subject, BindingResult bindingResult, ModelMap modelMap, RedirectAttributes redirectAttributes) {


        if (bindingResult.hasErrors()) {
            modelMap.addAttribute("subject", subject);
            return "subject/form";
        }

//        validate subject code
        Subject dupeCode = subjectDao.findByCode(subject.getCode());
        if(dupeCode != null && !dupeCode.getId().equalsIgnoreCase(subject.getId())) {

            modelMap.addAttribute("errorMessage", "Code duplicate form");
            modelMap.addAttribute("subject", subject);
            return "subject/form";
        }

        try {
            subjectDao.save(subject);
            redirectAttributes.addFlashAttribute("successMessage", "Data saved");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "redirect:/subject/list";
    }

    @GetMapping("/delete")
    public String deleteForm(@RequestParam String id, RedirectAttributes redirectAttributes) {
        try {
            subjectDao.deleteById(id);
            redirectAttributes.addFlashAttribute("successMessage", "Data is deleted");
        } catch (Exception e) {
            e.printStackTrace();
            redirectAttributes.addFlashAttribute("errorMessage", "Error deleteing data");
        }
        return "redirect:/subject/list";
    }
}
