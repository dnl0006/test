package com.artivisi.campus.entity;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table
@Data
public class Class extends BaseEntity{

    @NotEmpty(message = "Code must not empty")
    @Column(unique = true, length = 36)
    private String code;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "classes")
    private Set<Student> listStudent = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "classes")
    private Set<Schedule> listSchedule = new HashSet<>();
}
