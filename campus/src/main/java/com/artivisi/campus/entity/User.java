package com.artivisi.campus.entity;

import com.artivisi.campus.constant.Role;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table
@Data
public class User extends BaseEntity {

    @NotEmpty(message = "Username must not be empty")
    @Column(unique = true, length = 36)
    private String username;

    @NotEmpty(message = "Password must be filled")
    private String password;

    @Enumerated(EnumType.STRING) //agar tidak index array yg tersimpan
    @NotNull(message = "Role must be chosen")
    private Role role;

    @NotNull
    private Boolean active = Boolean.TRUE;

}
