package com.artivisi.campus.entity;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;;

@Table
@Entity
@Data
public class Schedule extends BaseEntity {

    @NotEmpty (message = "Start time must not empty")
    private String start;

    @NotEmpty(message = "End time must not empty")
    private String end;

    @NotEmpty(message = "Day must not empty")
    private String day;

    @NotNull(message = "Class must be chosen")
    @ManyToOne
    @JoinColumn(name = "id_class")
    private Class classes;

    @NotNull(message = "Teacher must be chosen")
    @ManyToOne
    @JoinColumn(name = "id_teacher")
    private Teacher teacher;

    @NotNull(message = "Subject must be chosen")
    @ManyToOne
    @JoinColumn(name = "id_subject")
    private Subject subject;

}
