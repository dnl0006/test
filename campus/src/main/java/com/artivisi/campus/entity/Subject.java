package com.artivisi.campus.entity;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;

@Entity
@Table
@Data
public class Subject extends BaseEntity {

    @NotEmpty (message = "Name must not empty")
    private String name;

    @NotEmpty(message = "Code must not empty")
    @Column(unique = true, length = 36)
    private String code;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
