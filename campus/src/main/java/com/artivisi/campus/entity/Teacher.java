package com.artivisi.campus.entity;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table
@Data
public class Teacher extends BaseEntity {

    @NotEmpty(message = "Nik must not empty")
    @Column(unique = true, length = 36)
    private String nik;

    @NotEmpty(message = "Name must not empty")
    private String name;

    @NotEmpty(message = "Address must not empty")
    private String address;

    @NotEmpty(message = "Phone must not empty")
    private String phone;

    @NotEmpty(message = "Email must not empty")
    private String email;

    @ManyToMany
    @OrderBy("code asc")
    @JoinTable(
            name = "teacher_subject",
            joinColumns = @JoinColumn(name = "id_teacher", nullable = false),
            inverseJoinColumns = @JoinColumn (name = "id_subject", nullable = false)
    )
    private Set<Subject> subjects = new HashSet<>();

    //ngecek apakah id yang di kirim di parameter ada di set<Subject> || mata kuliah
    public Boolean hasSubject(String id) {
        for (Subject s:subjects) {
            if (s.getId().equals(id)) {
                return true;
            }
        }
        return false;
    }
}
