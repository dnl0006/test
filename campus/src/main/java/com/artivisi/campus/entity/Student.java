package com.artivisi.campus.entity;


import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table
@Data
public class Student extends BaseEntity {

    @NotEmpty(message = "NIM must not empty")
    @Column(unique = true, length = 36)
    private String nim;

    @NotEmpty(message = "Name must not empty")
    private String name;

    private String address;

    private String phone;

    private String email;

    @NotNull(message = "Class must not be null")
    @ManyToOne
    @JoinColumn(name = "id_classes")
    private Class classes;

}
