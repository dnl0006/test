package com.artivisi.campus.dao;

import com.artivisi.campus.entity.User;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserDao extends PagingAndSortingRepository<User, String> {

    User findByUsername(String username);
}
