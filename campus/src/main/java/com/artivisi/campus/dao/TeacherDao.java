package com.artivisi.campus.dao;

import com.artivisi.campus.entity.Teacher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface TeacherDao extends PagingAndSortingRepository<Teacher, String> {

    Teacher findByNik(String nik);
    Page<Teacher> findByNikContainingIgnoreCase(String nik, Pageable pageable);
    Page<Teacher> findByNameContainingIgnoreCase(String name, Pageable pageable);


}
