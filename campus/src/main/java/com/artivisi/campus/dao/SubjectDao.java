package com.artivisi.campus.dao;

import com.artivisi.campus.entity.Subject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface SubjectDao extends PagingAndSortingRepository<Subject, String> {

    Subject findByCode(String code);
    Page<Subject> findByCodeContainingIgnoreCase(String code, Pageable pageable);
}
